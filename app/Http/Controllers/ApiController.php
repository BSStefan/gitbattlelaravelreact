<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getPopular(Request $request)
    {
        $language = $request->language;
        $url = 'https://api.github.com/search/repositories?q=stars:>1+language:' .
            $language . '&sort=stars&order=desc&type=Repositories';
        $client = new Client();
        $res = $client->get($url);
        return $res->getBody()->getContents();

    }

    public function getUser(Request $request)
    {
        $username = $request->username;

    }

    public function getRepo()
    {

    }
}

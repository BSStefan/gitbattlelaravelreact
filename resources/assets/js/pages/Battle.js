import React from 'react';
import ReactDOM from 'react-dom';
import PlayerForm from '../components/PlayerForm';
import PlayerProfil from '../components/PlayerProfil';


class Battle extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            PlayerOneUsername: null,
            PlayerOneImageUrl: null,
            PlayerTwoUsername: null,
            PlayerTwoImageUrl: null
        };
    }

    handleFindPlayer(username, i)
    {
        this.setState(() => {
            let newState = {};
            newState[i + 'Username'] = username;
            newState[i + 'ImageUrl'] = 'https://github.com/' + username + '.png?size=200';
            return newState;
        });
    }

    handleReset(i)
    {
        this.setState(() => {
            let newState = {};
            newState[i + 'Username'] = null;
            newState[i+ 'ImageUrl'] = null;
            return newState;
        });
    }

    render()
    {
        return(
            <div className="battle-container clearfix">
                <div className="clearfix">
                    {
                        this.state.PlayerOneUsername !== null ?
                            <PlayerProfil
                                username={this.state.PlayerOneUsername}
                                img={this.state.PlayerOneImageUrl}
                            >
                                <button
                                    className="player-form-button"
                                    onClick={() => this.handleReset("PlayerOne")}
                                >
                                    Reset
                                </button>
                            </PlayerProfil>
                            :
                            <PlayerForm
                                playerName="Player One"
                                findPlayer={(u, i) => this.handleFindPlayer(u, i)}
                            />
                    }
                    {
                        this.state.PlayerTwoUsername !== null ?
                            <PlayerProfil
                                username={this.state.PlayerTwoUsername}
                                img={this.state.PlayerTwoImageUrl}
                            >
                                <button
                                    className="player-form-button"
                                    onClick={() => this.handleReset("PlayerTwo")}
                                >
                                    Reset
                                </button>
                            </PlayerProfil>
                            :
                            <PlayerForm
                                playerName="Player Two"
                                findPlayer={(u, i) => this.handleFindPlayer(u, i)}
                            />
                    }
                </div>
                {
                    this.state.PlayerOneUsername !== null && this.state.PlayerTwoUsername !== null ?
                        <a
                            className="player-form-button"
                            href={'/battle/results?playerOne=' + this.state.PlayerOneUsername + '&playerTwo=' + this.state.PlayerTwoUsername}
                        >
                            Fight
                        </a>
                        :
                        null
                }
            </div>
        );
    }
}

export default Battle;

if(document.getElementById('battle')) {
    ReactDOM.render(<Battle/>, document.getElementById('battle'));
}

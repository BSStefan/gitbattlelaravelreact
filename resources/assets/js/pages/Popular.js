import React from 'react';
import ReactDOM from 'react-dom';
import NavLanguage from '../components/NavLanguages';
import api from '../utils/api';
import Repository from '../components/Repository';
import Loading from '../components/Loading';

class Popular extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            selectedLanguage: 'All',
            repos: null
        }
    }

    handleSelectLanguage(language)
    {
        this.setState(() => {
            return({
                selectedLanguage: language,
                repos: null
            });
        });

        api.fetchPopularRepos(language)
            .then((response) => {
                this.setState(() => {
                    return ({
                        selectedLanguage: language,
                        repos: response
                    });
                });
            });



    }

    componentDidMount()
    {
        this.handleSelectLanguage(this.state.selectedLanguage);
    }

    render()
    {
        return (
            <div className="popular-container">
                <NavLanguage
                    selectedLanguage={this.state.selectedLanguage}
                    selectLanguage={(language) => this.handleSelectLanguage(language)}
                />
                {this.state.repos !== null ? <Repository repos={this.state.repos} /> : <Loading item="Loading" />}
            </div>
        );
    }
}


export default Popular;

if(document.getElementById('popular')) {
    ReactDOM.render(<Popular/>, document.getElementById('popular'));
}
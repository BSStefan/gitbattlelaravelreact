import React from 'react';
import ReactDOM from 'react-dom';

class Home extends React.Component
{
    render()
    {
        return(
            <div className="home-container">
                <h1>Github Battle: Battle your friends... and stuff.</h1>

                <a href="/battle" className="home-button" >
                    Battle
                </a>
            </div>
        );
    }
}

export default Home;

if(document.getElementById('home')) {
    ReactDOM.render(<Home/>, document.getElementById('home'));
}
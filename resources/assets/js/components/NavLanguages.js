import React from 'react';
import PropTypes from 'prop-types';

function NavLanguages(props) {

    let languages = ['All', 'JavaScrip', 'Ruby', 'Java', 'Php'];

    return(
        <ul className="languages">
            {
                languages.map((language, index) => {

                    return (
                        <li key={language}
                            className={props.selectedLanguage === language ? 'active' : null }
                            onClick={() => props.selectLanguage(language)}
                        >
                            {language}
                        </li>
                    );
                })
            }
        </ul>
    );
}

NavLanguages.propTypes = {
    selectedLanguage: PropTypes.string.isRequired,
    selectLanguage: PropTypes.func.isRequired
};

export default NavLanguages;
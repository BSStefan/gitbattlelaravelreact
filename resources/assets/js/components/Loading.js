import React from 'react';
import PropTypes from 'prop-types';

class Loading extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            animation: props.item,
            endAnimation : props.item + '...'
        }
    }

    componentDidMount()
    {
        this.time = setInterval(() => {
            this.setState(() => {
                if(this.state.animation !== this.state.endAnimation) {
                    return {
                        animation: this.state.animation + '.'
                    }
                }
                else {
                    return {
                        animation: this.props.item
                    }
                }
            });
        }, 300);
    }

    componentWillUnmount()
    {
        clearInterval(this.time);
    }

    render()
    {
        return(
            <div className="loading">
                {this.state.animation}
            </div>
        );
    }
}

Loading.propTypes = {
    item: PropTypes.string.isRequired
};

export default Loading;

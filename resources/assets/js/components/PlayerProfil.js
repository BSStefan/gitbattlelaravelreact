import React from 'react';
import propTypes from 'prop-types';

class PlayerProfil extends  React.Component
{
    render()
    {
        return(
            <div className="player-profil-container">
                <img src={this.props.img} alt={this.props.username}/>
                <p>{this.props.username}</p>
                <div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

PlayerProfil.PropTypes = {
    img: propTypes.string.isRequired,
    username: propTypes.string.isRequired
};

export default PlayerProfil;

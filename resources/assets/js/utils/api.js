import axios from 'axios';
delete axios.defaults.headers.common['x-csrf-token'];


function getProfile (username)
{
    return axios.get('https://api.github.com/users/' + username )
        .then(function (user) {
            return user.data;
        });
}

function getRepos (username)
{
    return axios.get('https://api.github.com/users/' + username + '/repos?per_page=100');
}

function getStarCount (repos)
{
    return repos.data.reduce(function (count, repo) {
        return count + repo.stargazers_count
    }, 0);
}

function calculateScore (profile, repos) {
    let followers = profile.followers;
    let totalStars = getStarCount(repos);

    return (followers * 3) + totalStars;
}

function getUserData (player) {
    return axios.all([
        getProfile(player),
        getRepos(player)
    ]).then(function (data) {
        let profile = data[0];
        let repos = data[1];

        return {
            profile: profile,
            score: calculateScore(profile, repos)
        }
    });
}

function compireUser(users) {
    return users.sort((a, b) => {
        return b.score - a.score;
    });
}

function handleerror(error) {
    console.log(error);
    return null;
}



const api = {



    /*
    * call my api
    * https://api.github.com/search/repositories?q=stars:>1+language:'
     + language +'&sort=stars&order=desc&type=Repositories');
    * **/
    fetchPopularRepos: (language) => {
        let encodedUri = window.encodeURI('http://localhost:8002/api/popular?language='+language);
        //delete axios.defaults.headers.common['X-CSRF-TOKEN'];

        return axios.get(encodedUri)
            .then((response) => {
                return response.data.items;
            });
    },

    battle: (users) => {
        return axios.all(users.map(getUserData))
            .then(compireUser)
            .catch(handleerror);
    }

};


export default api;
